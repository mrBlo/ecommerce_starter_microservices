package com.blo.web.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blo.web.controller.ProductController;
import com.blo.web.entity.Product;
import com.blo.web.exception.ProductNotFound;
import com.blo.web.repository.ProductRepository;

@Service
public class ProductService {
	private ProductRepository productRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);
	
	@Autowired
	public ProductService(ProductRepository productRepository) {
		super();
		this.productRepository = productRepository;
	}

	public List<Product> getAllProducts(){
		 List<Product>allProducts= new ArrayList<>();
		 productRepository.findAll().forEach(
				 product->{
					 allProducts.add(product);
				 });
		 
		 return allProducts; 
		}
	

	public Product findProductById(int id) {
		Product product =this.productRepository.findById(id);
		if(product==null) {
			LOGGER.warn("PRODUCT [ID= " + id + "] NOT FOUND");
			throw new ProductNotFound();		
		}
		return product;	
	}
	
	public Product createProduct(Product product) {
		return this.productRepository.save(product); 
	}
	
	public void deleteProduct(Product product) {
		 this.productRepository.delete(product);
	}
	
	

	
}
