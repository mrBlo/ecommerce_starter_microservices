package com.blo.web.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.Pattern;

import org.springframework.hateoas.RepresentationModel;


import lombok.Data;


@Entity
@Data
@Table(name = "product")
public class Product extends RepresentationModel<Product>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name="ID")
	private int id;
	
	@Column(name="name")
	@NotBlank(message="Please provide a product name")
  //  @Pattern(regexp="^$|[a-zA-Z ]+$", message="Name must not include special characters.")
	private String name;
	
	 @DecimalMin(value = "0.0", inclusive = false)
	 @Digits(integer=6, fraction=2) // 999999, 99.99
	private BigDecimal price;
	
	 @Column(name="description")
	private String description;

	public Product(int id, String name, BigDecimal price, String description) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
	}

	public Product() {
		super();
	} 
	
	
	

}
