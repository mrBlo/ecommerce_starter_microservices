package com.blo.web.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.blo.web.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

	Product findById(int id);
	

}
