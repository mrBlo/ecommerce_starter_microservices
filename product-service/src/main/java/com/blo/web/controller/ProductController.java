package com.blo.web.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blo.web.entity.Product;
import com.blo.web.exception.ProductNotFound;
import com.blo.web.repository.ProductRepository;
import com.blo.web.service.ProductService;

@RestController
@RequestMapping(value = "/api/v1")
public class ProductController {
	@Autowired
	private ProductService productService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
	@Value("${my.name: Config Server is not working. Please check...}")
    private String msg;
	
	
	//Endpoint to test config server
    @GetMapping(value = "/blo")
	public String getTrail(){
		return msg;
	}
    
    
	@GetMapping(value = "/products")
	public List<Product> getAll(){
		LOGGER.info("GETTING ALL PRODUCTS");
		List<Product> allProducts= productService.getAllProducts();
		for(Product product: allProducts) {
			product.add(linkTo(ProductController.class).slash("products").slash(product.getId()).withSelfRel());
		}
		
		return allProducts;
	}
	
	
	@GetMapping(value="/products/{id}")
	public Product getProduct(@PathVariable (value = "id") int id ){
		LOGGER.info("GETTING SINGLE PRODUCT [ID= " + id + "]");
		Product product= productService.findProductById(id);
		//	if (product==null) 	throw new ProductNotFound(); //handled in Service
			 //implementing HATEOAS GET allProducts link here
			 Link allProductsLink = linkTo(methodOn(ProductController.class).getAll()).withRel("ALL PRODUCTS");
			 product.add(allProductsLink);
		return product;
	}
	
	
	  

	@PostMapping(value="/products" , consumes =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> post(@Valid @RequestBody Product product){
		LOGGER.info("INITIATING POST OF PRODUCT [DETAILS= " + product + "]");
		 productService.createProduct(product);
		 LOGGER.info("PRODUCT [DETAILS= " + product + "] POSTED SUCCESSFULLY");
		 return new ResponseEntity<Object>(product, HttpStatus.CREATED);
	}
	 
	 
	 @DeleteMapping(value="/products/{id}")
	  public Map<String, Link> delete(@PathVariable(value = "id") int id){
			LOGGER.info("INITIATING DELETION OF PRODUCT [ID= " + id + "]");
		 Product product = productService.findProductById(id);
		    productService.deleteProduct(product);
			LOGGER.info("PRODUCT [DETAILS= " + product + "]" + " DELETED SUCCESSFULLY");
			
	  //implementing HATEOAS GET allProducts link here
			 Link allProductsLink = linkTo(methodOn(ProductController.class).getAll()).withRel("ALL PRODUCTS");
	    Map<String, Link> response = new HashMap<>();
	    response.put("Product deleted",allProductsLink);
	    return response;
	  }
	 
	 
	 
	 
	 @PutMapping(value="/products/{id}", consumes =  MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Product> put(@Valid @RequestBody Product newProduct, @PathVariable (value = "id")  int id) {
			
			LOGGER.info("INITIATING UPDATE OF PRODUCT [ID= " + id + "]");
		 Product product = productService.findProductById(id); 
			product.setName(newProduct.getName());
			product.setPrice(newProduct.getPrice());
			product.setDescription(newProduct.getDescription());
			Product updatedProduct = productService.createProduct(product);
			LOGGER.info("PRODUCT [ID= " + id + "]" + " UPDATED SUCCESSFULLY");
			
			//implementing HATEOAS self link here
			 Link selfLink = linkTo(ProductController.class).slash("products").slash(updatedProduct.getId()).withSelfRel();
			 updatedProduct.add(selfLink);
			return ResponseEntity.ok(updatedProduct);
		}


}
