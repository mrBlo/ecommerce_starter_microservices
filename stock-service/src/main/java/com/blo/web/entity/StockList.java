package com.blo.web.entity;

import java.util.List;

import lombok.Data;

@Data
public class StockList {

	List<Stock> stockList;
	
	
}
