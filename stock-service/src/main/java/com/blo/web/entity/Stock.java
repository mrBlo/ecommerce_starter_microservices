package com.blo.web.entity;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.hateoas.RepresentationModel;

import lombok.Data;

@Entity
@Data
@Table(name = "stock")
public class Stock extends RepresentationModel<Stock> {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name="ID")
	private int id;
	
	@Column(name="PRODUCT_ID")
	 @Min(value = 1, message = "Product ID should not be less than 1")
	@NotNull(message = "Please provide a product ID")
	private int productId;

	@Column(name="IN_STOCK")
	 @Min(value = 1, message = "Number of product items in stock should not be less than 1")
	@NotNull(message = "Please provide the number of product items in stock")
	private int numberInStock;

	@Column(name="UPDATED_ON")
	@UpdateTimestamp
	private Date updatedOn;

	public Stock() {
		super();
	}

	public Stock(int id,  int productId,
			 int numberInStock,
			Date updatedOn) {
		super();
		this.id = id;
		this.productId = productId;
		this.numberInStock = numberInStock;
		this.updatedOn = updatedOn;
	}
	
	
	
	
	
	
	
	
}
