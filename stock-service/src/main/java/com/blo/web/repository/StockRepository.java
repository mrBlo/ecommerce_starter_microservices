package com.blo.web.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.blo.web.entity.Stock;

@Repository
public interface StockRepository extends CrudRepository<Stock, Integer>{
	
	Stock findById(int id);
	
	 @Query(value = "SELECT COUNT(PRODUCT_ID) FROM stock WHERE PRODUCT_ID = ?1",nativeQuery =true)
	 long getStockCountbyProductId(int productId);

	
	
}
