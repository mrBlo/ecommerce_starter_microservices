package com.blo.web.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blo.web.entity.Stock;
import com.blo.web.entity.StockList;
import com.blo.web.exception.ProductIdAlreadyExists;
import com.blo.web.service.StockService;


@RestController
@RequestMapping(value = "/api/v1")
public class StockController {
	@Autowired
	private StockService stockService;
	private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);
	
	@Value("${my.name: Config Server is not working. Please check...}")
    private String msg;
	
	
	
	//Endpoint to test config server
    @GetMapping(value = "/blo")
	public String getTrail(){
		return msg;
	}
	
	/*
	 * It's good practice not to return a List when outputting via REST,
	 instead return an object that contains a list 
	 */
	@GetMapping(value = "/stocks")
	public StockList getAll(){
		LOGGER.info("GETTING ALL STOCK ITEMS");
		List<Stock> allStock= stockService.getAllStock();
		for(Stock stock: allStock) {
			stock.add(linkTo(StockController.class).slash("stocks").slash(stock.getId()).withSelfRel());
		}
		StockList stockList=new StockList();
		stockList.setStockList(allStock);
		return stockList;
	}
	
	
	
	@GetMapping(value="/stocks/{id}")
	public Stock getStock(@PathVariable (value = "id") int id ){
		LOGGER.info("GETTING SINGLE STOCK ITEM [ID= " + id + "]");
		Stock stock= stockService.findStockById(id);
			//if (stock==null) throw new StockNotFound(); //handled in Service
			 //implementing HATEOAS GET allStock link here
			 Link allStockLink = linkTo(methodOn(StockController.class).getAll()).withRel("ALL STOCK ITEMS");
			 stock.add(allStockLink);
		return stock;
	}
	
	
	  

	@PostMapping(value="/stocks" , consumes =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> post(@Valid @RequestBody Stock stock){
		LOGGER.info("INITIATING POST OF STOCK ITEM [DETAILS= " + stock + "]");
		if (stockService.productIdExists(stock.getProductId())) {
			LOGGER.warn("STOCK WITH PRODUCT [ID= " + stock.getProductId() + "] ALREADY EXISTS");
			throw new ProductIdAlreadyExists();
		}
		 stockService.createStock(stock);
		 LOGGER.info("STOCK ITEM [DETAILS= " + stock + "] POSTED SUCCESSFULLY");
		return new ResponseEntity<>(HttpStatus.CREATED);	
	}
	 
	 
	 @DeleteMapping(value="/stocks/{id}")
	  public Map<String, Link> delete(@PathVariable(value = "id") int id){
		 LOGGER.info("INITIATING DELETION OF STOCK ITEM [ID= " + id + "]");
		 Stock stock = stockService.findStockById(id);
		    stockService.deleteStock(stock);
		    LOGGER.info("STOCK ITEM [STOCK= " + stock + "]" + " DELETED SUCCESSFULLY");
	  //implementing HATEOAS GET allStock link here
			 Link allStockLink = linkTo(methodOn(StockController.class).getAll()).withRel("ALL STOCK ITEMS");
	    Map<String, Link> response = new HashMap<>();
	    response.put("Stock deleted",allStockLink);
	    return response;
	  }
	 
	 
	 
	 
	 @PutMapping(value="/stocks/{id}", consumes =  MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Stock> put(@Valid @RequestBody Stock newStock, @PathVariable (value = "id")  int id) {
			
		 LOGGER.info("INITIATING UPDATE OF STOCK ITEM [ID= " + id + "]");		
		 Stock stock = stockService.findStockById(id); 
		
		//check for different productId and throw exception if new productId already exists in system
			if(stockService.isNewProductId(newStock.getProductId(), stock.getProductId())) {
				LOGGER.info("NEW PRODUCT ID [ "+newStock.getProductId()+" ] DETECTED");
				if (stockService.productIdExists(newStock.getProductId())) {
					LOGGER.warn("NEW PRODUCT ID [" + newStock.getProductId() + "] ALREADY EXISTS");
					throw new ProductIdAlreadyExists();
				}
				
			}
			//if(newStock.getProductId()!=0 )
				stock.setProductId(newStock.getProductId());
				

			
			if(newStock.getNumberInStock()!=0 )
			stock.setNumberInStock(newStock.getNumberInStock());
		
			Stock updatedStock = stockService.createStock(stock);
			LOGGER.info("STOCK ITEM [ID= " + id + "]" + " UPDATED SUCCESSFULLY");

			//implementing HATEOAS self link here
			 Link selfLink = linkTo(StockController.class).slash("stocks").slash(updatedStock.getId()).withSelfRel();
			 updatedStock.add(selfLink);
			return ResponseEntity.ok(updatedStock);
		}


}
