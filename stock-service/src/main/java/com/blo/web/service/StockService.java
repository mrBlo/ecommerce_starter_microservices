package com.blo.web.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blo.web.entity.Stock;
import com.blo.web.exception.StockNotFound;
import com.blo.web.repository.StockRepository;

@Service
public class StockService {

	@Autowired
	private StockRepository stockRepository;
	private static final Logger LOGGER = LoggerFactory.getLogger(StockService.class);
	
	
	public List<Stock> getAllStock(){
		 List<Stock>allStock= new ArrayList<>();
		 stockRepository.findAll().forEach(
				 stock->{
					 allStock.add(stock);
				 });
		 
		 return allStock; 
		}
	

	public Stock findStockById(int id) {
		Stock stock =this.stockRepository.findById(id);
		if(stock==null) {
			LOGGER.warn("STOCK ITEM [ID= " + id + "] NOT FOUND");
			throw new StockNotFound();		
		}
		return stock;	
	}
	
	public Stock createStock(Stock stock) {
		return this.stockRepository.save(stock); 
	}
	
	public void deleteStock(Stock stock) {
		 this.stockRepository.delete(stock);
	}
	
	public boolean productIdExists(int productId) {	
		long stockCount = stockRepository.getStockCountbyProductId(productId);
		if(stockCount>0) {
			return true;
		}
		return false;
	}
	
	public boolean isNewProductId(int newProductId,int existingProductId) {
		if (newProductId==existingProductId)
			return false;
		return true;		
	}
		
	
}
