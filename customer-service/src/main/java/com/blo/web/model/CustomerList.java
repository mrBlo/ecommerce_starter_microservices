package com.blo.web.model;

import java.util.List;

import com.blo.web.entity.Customer;

import lombok.Data;

@Data
public class CustomerList {
	private List<Customer>customers;

}
