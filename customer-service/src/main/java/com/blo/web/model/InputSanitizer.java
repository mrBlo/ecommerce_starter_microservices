package com.blo.web.model;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class InputSanitizer {
	//cleanup for text only
	 public static String cleanIt(String arg0) {
	        return Jsoup.clean(
	                StringEscapeUtils.escapeHtml(StringEscapeUtils.escapeJavaScript(StringEscapeUtils.escapeSql(arg0)))
	                , Whitelist.none());
	    }


}
