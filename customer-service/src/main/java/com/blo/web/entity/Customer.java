package com.blo.web.entity;


//import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.hateoas.RepresentationModel;

//import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Data
@Table(name = "ecommerce_customer")
public class Customer extends RepresentationModel<Customer> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name="id")
	private int id;
	
	@Column(name="first_name")
	@NotBlank(message="First name is mandatory")
	@Size(max=30,message = "First name should not exceed 30 characters")
	@Pattern(regexp="^$|[a-zA-Z ]+$", message="First name must not include special characters.")
	private String firstName;
	
	@Column(name="last_name")
	@Size(max=30,message = "Last name should not exceed 30 characters")
	@NotBlank(message="Last name is mandatory")
	@Pattern(regexp="^$|[a-zA-Z ]+$", message="Last name must not include special characters.")
	private String lastName;
	
	@Column(name="email")
	@NotBlank(message="Email address is mandatory")
	@Email(message = "Please provide a valid email address")
	private String email;
	
	@Column(name="phone")
//	@Size(min=10,max=10,message = "Please provide a valid 10 digit phone number")
//	@NotBlank(message="Phone number is mandatory")
//	@Pattern(regexp="^[0-9]*$", message = "Phone number cannot include characters")
	@Pattern(regexp="(0)[0-9]{9}", message = "Please provide a valid 10 digit phone number")
	private String phone;
	
	@Column(name="created")
	@CreationTimestamp
	private Date createdOn;
	
	@Column(name="updated")
	@UpdateTimestamp
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updatedOn;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(int id,String firstName, String lastName, String email, String phone,
			Date createdOn, Date updatedOn) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
	}
	
	
	
	
	
	
}
