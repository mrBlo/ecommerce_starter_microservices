package com.blo.web.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.blo.web.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer>{
	
	Customer findById(int id);

	boolean existsCustomerByEmail(String email);
	

}
