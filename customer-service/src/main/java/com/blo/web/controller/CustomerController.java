package com.blo.web.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blo.web.entity.Customer;
import com.blo.web.exception.CustomerAlreadyExists;
import com.blo.web.exception.CustomerNotFound;
import com.blo.web.model.CustomerList;
import com.blo.web.service.CustomerService;


@RestController
@RequestMapping(value = "/api/v1")
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	private Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

	
	@Value("${my.name: Config Server is not working. Please check...}")
    private String msg;
	
	//Endpoint to test config server
    @GetMapping(value = "/blo")
	public String getTrail(){
		return msg;
	}
	
	@GetMapping(value = "/customers")
	public CustomerList getAll(){
		LOGGER.info("GETTING ALL CUSTOMERS");
		List<Customer> allCustomers= customerService.getAllCustomers();
		allCustomers.stream().forEach(
			customer->customer.add(linkTo(CustomerController.class).slash("customers").slash(customer.getId()).withSelfRel()));
		CustomerList customers=new CustomerList();
		customers.setCustomers(allCustomers);
		return customers;
	}
	
	
	
	@GetMapping(value="/customers/{id}")
	public Customer getCustomer(@PathVariable (value = "id") int id ){
		LOGGER.info("GETTING SINGLE CUSTOMER [ID= " + id + "]");
		Customer customer= customerService.findCustomerById(id);
			//if (customer==null) throw new CustomerNotFound(); //handled in Service
			 //implementing HATEOAS GET allCustomers link here
			 Link allCustomersLink = linkTo(methodOn(CustomerController.class).getAll()).withRel("CUSTOMERS");
			 customer.add(allCustomersLink);
		return customer;
	}
	
	
	  

	@PostMapping(value="/customers" , consumes =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> post(@Valid @RequestBody Customer customer){
		LOGGER.info("INITIATING POST OF CUSTOMER [DETAILS= " + customer + "]");
		//sanitizing inputs
		//String whatever = InputSanitizer.cleanIt(whatever); 
		if (customerService.emailExists(customer.getEmail())) {
			LOGGER.warn("CUSTOMER EMAIL [" + customer.getEmail() + "] ALREADY EXISTS");
			throw new CustomerAlreadyExists();
		}
		customerService.createCustomer(customer);
		 LOGGER.info("CUSTOMER [DETAILS= " + customer + "] POSTED SUCCESSFULLY");
		return new ResponseEntity<>(HttpStatus.CREATED);	
	}
	 
	 
	 @DeleteMapping(value="/customers/{id}")
	  public Map<String, Link> delete(@PathVariable(value = "id") int id){
		 LOGGER.info("INITIATING DELETION OF CUSTOMER [ID= " + id + "]");
		 Customer customer = customerService.findCustomerById(id);
	
		    customerService.deleteCustomer(customer);
		    LOGGER.info("CUSTOMER [DETAILS= " + customer + "]" + " DELETED SUCCESSFULLY");
	  //implementing HATEOAS GET allCustomers link here
		    Link allCustomersLink = linkTo(methodOn(CustomerController.class).getAll()).withRel("CUSTOMERS");
	    Map<String, Link> response = new HashMap<>();
	    response.put("Customer deleted",allCustomersLink);
	    return response;
	  }
	 
	 
	 
	 
	 @PutMapping(value="/customers/{id}", consumes =  MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Customer> put(@Valid @RequestBody Customer newCustomer, @PathVariable (value = "id")  int id) {
		 LOGGER.info("INITIATING UPDATE OF CUSTOMER [ID= " + id + "]");
		 Customer customer = customerService.findCustomerById(id); 
			
			customer.setFirstName(newCustomer.getFirstName());
			customer.setLastName(newCustomer.getLastName());
			
			//check for different email and throw exception if new email exists in system
			if(customerService.isNewEmail(newCustomer.getEmail(), customer.getEmail())) {
				LOGGER.info("NEW CUSTOMER EMAIL [ "+newCustomer.getEmail()+" ] DETECTED");
				if (customerService.emailExists(newCustomer.getEmail())) {
					LOGGER.warn("CUSTOMER EMAIL [" + newCustomer.getEmail() + "] ALREADY EXISTS");
					throw new CustomerAlreadyExists();
				}
			}	
			customer.setEmail(newCustomer.getEmail());
			customer.setPhone(newCustomer.getPhone());
			
//			if(newStock.getNumberInStock()!=0 )
//			stock.setNumberInStock(newStock.getNumberInStock());
		
			Customer updatedCustomer = customerService.createCustomer(customer);
			LOGGER.info("CUSTOMER [ID= " + id + "]" + " UPDATED SUCCESSFULLY");
			//implementing HATEOAS self link here
			 Link selfLink = linkTo(CustomerController.class).slash("customers").slash(updatedCustomer.getId()).withSelfRel();
			 LOGGER.error(selfLink.toString());
			 updatedCustomer.add(selfLink);
			return ResponseEntity.ok(updatedCustomer);
		}



}
