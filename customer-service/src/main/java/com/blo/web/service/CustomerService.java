package com.blo.web.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blo.web.entity.Customer;
import com.blo.web.exception.CustomerAlreadyExists;
import com.blo.web.exception.CustomerNotFound;
import com.blo.web.repository.CustomerRepository;


@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);
	
	
	public List<Customer> getAllCustomers(){
		 List<Customer>allCustomers= new ArrayList<>();
		 customerRepository.findAll().forEach(
				 customer->{
					 allCustomers.add(customer);
				 });
		 
		 return allCustomers; 
		}
	

	public Customer findCustomerById(int id) {
		Customer customer =this.customerRepository.findById(id);
		if(customer==null) {
			LOGGER.warn("CUSTOMER [ID= " + id + "] NOT FOUND");
			throw new CustomerNotFound();		
		}
		return customer;	
	}
	
	public Customer createCustomer(Customer customer) {
		return this.customerRepository.save(customer); 
	}
	
	public void deleteCustomer(Customer customer) {
		 this.customerRepository.delete(customer);
	}
	
	public boolean emailExists(String email) {	
		return customerRepository.existsCustomerByEmail(email);
	}
	
	public boolean isNewEmail(String newCustomerEmail,String existingCustomerEmail) {
		if (newCustomerEmail.equalsIgnoreCase(existingCustomerEmail))
			return false;
		return true;		
	}
		
	
}
