package com.blo.web.entity;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.hateoas.RepresentationModel;

import com.blo.web.model.OrderStatus;

import lombok.Data;

@Entity
@Data
@Table(name = "ecommerce_order")
public class Order extends RepresentationModel<Order>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name="id")
	private int id;
	
	@Column(name="product_id")
	@NotNull(message = "Product ID is mandatory")
	private int productId;
	
	@Column(name="customer_id")
	@NotNull(message = "Customer ID is mandatory")
	private int customerId;
	
	@Column(name="ordered_on")
	@CreationTimestamp
	private Date orderedOn;
	
	@Column(name="quantity")
	@NotNull(message = "Quantity is mandatory")
	private int quantity;
	
	@Column(name="status")
	@NotNull(message = "Order Status is mandatory")
	private OrderStatus orderStatus;
	
	@Column(name="price_each")
	@NotNull(message = "Product Price is mandatory")
	 @DecimalMin(value = "0.0", inclusive = false)
	 @Digits(integer=6, fraction=2) // 999999, 99.99
	private BigDecimal priceEach;
	
	@Column(name="total_price")
	@NotNull(message = "Total Price is mandatory")
	 @DecimalMin(value = "0.0", inclusive = false)
	 @Digits(integer=6, fraction=2) // 999999, 99.99
	private BigDecimal totalPrice;
	
	

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Order(int id,  int productId,int customerId, Date orderedOn,
			 int quantity, OrderStatus orderStatus, BigDecimal priceEach,BigDecimal totalPrice) {
		super();
		this.id = id;
		this.productId = productId;
		this.customerId = customerId;
		this.orderedOn = orderedOn;
		this.quantity = quantity;
		this.orderStatus = orderStatus;
		this.priceEach = priceEach;
		this.totalPrice = totalPrice;
	}


	
	
	
}
