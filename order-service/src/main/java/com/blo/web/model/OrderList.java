package com.blo.web.model;

import java.util.List;

import com.blo.web.entity.Order;

import lombok.Data;

@Data
public class OrderList {
	private List<Order>orders;
}
