package com.blo.web.model;

public enum OrderStatus {
	
	PLACED,
	APPROVED,
	PAID,
	DELIVERED

}
