package com.blo.web.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blo.web.entity.Order;
import com.blo.web.exception.OrderNotFound;
import com.blo.web.model.OrderList;
import com.blo.web.service.OrderService;

@RestController
@RequestMapping(value = "/api/v1")
public class OrderController {

	@Autowired
	private OrderService orderService;

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

	@Value("${my.name: Config Server is not working. Please check...}")
	private String msg;

	// Endpoint to test config server
	@GetMapping(value = "/blo")
	public String getTrail() {
		return msg;
	}

	@GetMapping(value = "/orders")
	public OrderList getAll() {
		LOGGER.info("GETTING ALL ORDERS");
		List<Order> allOrders = orderService.getAllOrders();
		allOrders.stream().forEach(order -> order.add(linkTo(OrderController.class)
				.slash("orders").slash(order.getId()).withSelfRel()));
		OrderList orders = new OrderList();
		orders.setOrders(allOrders);
		return orders;
	}

	@GetMapping(value = "/orders/{id}")
	public Order getOrder(@PathVariable(value = "id") int id) {
		LOGGER.info("GETTING SINGLE ORDER [ID= " + id + "]");
		Order order = orderService.findOrderById(id);
		//if (order == null) throw new OrderNotFound(); //handled in Service
	
		// implementing HATEOAS GET allOrders link here
		Link allOrdersLink = linkTo(methodOn(OrderController.class).getAll()).withRel("ORDERS");
		order.add(allOrdersLink);
		return order;
	}

	@PostMapping(value = "/orders", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> post(@Valid @RequestBody Order order) {
		LOGGER.info("INITIATING POST OF ORDER [DETAILS= " + order + "]");
		orderService.createOrder(order);
		LOGGER.info("ORDER [DETAILS= " + order + "] POSTED SUCCESSFULLY");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/orders/{id}")
	public Map<String, Link> delete(@PathVariable(value = "id") int id) {

		LOGGER.info("INITIATING DELETION OF ORDER [ID= " + id + "]");
		Order order = orderService.findOrderById(id);
		orderService.deleteOrder(order);
		LOGGER.info("ORDER [DETAILS= " + order + "]" + " DELETED SUCCESSFULLY");
		// implementing HATEOAS GET allOrders link here
		Link allOrdersLink = linkTo(methodOn(OrderController.class).getAll()).withRel("ORDERS");
		Map<String, Link> response = new HashMap<>();
		response.put("Order deleted", allOrdersLink);
		return response;
	}

	@PutMapping(value = "/orders/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Order> put(@Valid @RequestBody Order newOrder, @PathVariable(value = "id") int id) {

		LOGGER.info("INITIATING UPDATE OF ORDER [ID= " + id + "]");
		Order order = orderService.findOrderById(id);
		// not editing the productId and customerId because they are linked with the
		// orderId

		if (newOrder.getQuantity() != 0)
			order.setQuantity(newOrder.getQuantity());

		order.setOrderStatus(newOrder.getOrderStatus());
		order.setPriceEach(newOrder.getPriceEach());
		order.setTotalPrice(newOrder.getTotalPrice());

		Order updatedOrder = orderService.createOrder(order);
		LOGGER.info("ORDER [ID= " + id + "]" + " UPDATED SUCCESSFULLY");
		// implementing HATEOAS self link here
		Link selfLink = linkTo(OrderController.class).slash("orders").slash(updatedOrder.getId())
				.withSelfRel();
		updatedOrder.add(selfLink);
		return ResponseEntity.ok(updatedOrder);
	}

}
