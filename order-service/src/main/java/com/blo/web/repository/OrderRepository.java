package com.blo.web.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.blo.web.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer>{
	
	Order findById(int OrderId);
}
