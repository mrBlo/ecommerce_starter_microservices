package com.blo.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import brave.sampler.Sampler;

@SpringBootApplication
@EnableEurekaClient
public class OrderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}
	
	/*
	 * Sampler determines what amount of data you want to send to a centralized log analysis tool.
	 *  that is if you want to send all the data or only a part of it. There is  an AlwaysSampler that 
	 *  exports everything and a ProbabilityBasedSampler that samples a fixed fraction of spans.
	 *   We will be making use of the Always Sampler for this example.
	 */
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
	//or use "spring.sleuth.sampler.probability=100" in .props file


}
