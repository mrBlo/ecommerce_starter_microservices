package com.blo.web.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blo.web.entity.Order;
import com.blo.web.exception.OrderNotFound;
import com.blo.web.repository.OrderRepository;


@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);
	
	
	public List<Order> getAllOrders(){
		 List<Order>allOrders= new ArrayList<>();
		 orderRepository.findAll().forEach(
				 order->{
					 allOrders.add(order);
				 });
		 
		 return allOrders; 
		}
	

	public Order findOrderById(int id) {
		Order order =this.orderRepository.findById(id);
		if(order==null) {
			LOGGER.warn("ORDER [ID= " + id + "] NOT FOUND");
			throw new OrderNotFound();		
		}
		return order;	
	}
	
	public Order createOrder(Order order) {
		return this.orderRepository.save(order); 
	}
	
	public void deleteOrder(Order order) {
		 this.orderRepository.delete(order);
	}
	
		
}
