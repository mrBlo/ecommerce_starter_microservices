package com.blo.web.controller.hytsrix;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.blo.web.exception.ProductStockNotFound;
import com.blo.web.model.Stock;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

//Created this class so Hystrix can pick up this class and its fallback method

@Component //you can use @Service too
public class SingleStockServiceCall {

	@Autowired
	private RestTemplate restTemplate;
	private static final String STOCK_SERVICE_URL="http://stock-service/api/v1/stocks/";
	private static final Logger LOGGER = LoggerFactory.getLogger(SingleStockServiceCall.class);



@HystrixCommand(fallbackMethod = "getFallbackSingleStock"
//,
//			commandProperties = {
//		@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000"),
//		@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "6"),
//		@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
//		@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000")
//										}
//,
//				//Creating Bulkhead config properties
//			threadPoolKey = "stockServicePool",
//			threadPoolProperties = {
//		@HystrixProperty(name = "coreSize",value="30"), //size of the thread pool
//		@HystrixProperty(name = "maxQueueSize",value="15") //max size of threads in  queue
//									}					
)
public Stock getSingleStock(int stockId) {
	LOGGER.info("GETTING SINGLE STOCK ITEM [ID="+stockId+"] USING RESTTEMPLATE CALL TO STOCK-SERVICE");
	//get the Stock from calling the stock-service using the entered stockId
	Stock stock= restTemplate
			.getForObject(STOCK_SERVICE_URL+stockId, Stock.class);
	if(stock==null) {
		LOGGER.warn("STOCK ITEM [ID="+stockId+"] IS NULL HENCE ProductStockNotFound MSG SHOWN TO USER");
		throw new ProductStockNotFound();
	}
	return stock;
}


		/* FALLBACK METHOD*/
public Stock getFallbackSingleStock(int stockId) {
	LOGGER.warn("INITIATING FALLBACK METHOD FOR GETTING STOCK ITEM [ID="+stockId+"] FROM STOCK-SERVICE");
	////// do cache here later
	Stock stock= new Stock(0, 0, 0, new Date());
	LOGGER.info("FALLBACK FOR STOCK ITEM [ID="+stockId+"] EXECUTED");
return stock;
}

}
