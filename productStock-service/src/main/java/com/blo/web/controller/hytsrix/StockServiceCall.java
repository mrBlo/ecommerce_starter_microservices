package com.blo.web.controller.hytsrix;

import java.util.Arrays;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.blo.web.model.Stock;
import com.blo.web.model.StockList;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

//Created this class so Hystrix can pick up this class and its fallback method

@Component //you can use @Service too
public class StockServiceCall {

	@Autowired
	private RestTemplate restTemplate;
	private static final String STOCK_SERVICE_URL="http://stock-service/api/v1/stocks/";
	private static final Logger LOGGER = LoggerFactory.getLogger(StockServiceCall.class);


@HystrixCommand(fallbackMethod = "getFallbackStockList"
//,
//			commandProperties = {
//		@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000"),
//		@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "6"),
//		@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
//		@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000")
//										}
//,
//				//Creating Bulkhead config properties
//			threadPoolKey = "stockServicePool",
//			threadPoolProperties = {
//		@HystrixProperty(name = "coreSize",value="30"), //size of the thread pool
//		@HystrixProperty(name = "maxQueueSize",value="15") //max size of threads in  queue
//									}					
)
public StockList getStockList() {
	LOGGER.info("GETTING ALL STOCK ITEMS LIST USING RESTTEMPLATE CALL TO STOCK-SERVICE");
	//get StockList, which is a List Object, from calling the stock-service
			StockList stockList= restTemplate
					.getForObject(STOCK_SERVICE_URL, StockList.class);
	return stockList;
}


/* FALLBACK METHOD*/
public StockList getFallbackStockList() {
	////// do cache here later
	LOGGER.warn("INITIATING FALLBACK METHOD FOR GETTING ALL STOCK ITEMS FROM STOCK-SERVICE");
StockList stockList= new StockList();
stockList.setStockList(Arrays.asList(
		new Stock(0, 0, 0, new Date())));
LOGGER.info("FALLBACK FOR ALL STOCK ITEMS EXECUTED");
return stockList;
}


	
}
