package com.blo.web.controller.hytsrix;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.blo.web.model.Product;
import com.blo.web.model.ProductStockItem;
import com.blo.web.model.Stock;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

//Created this class so Hystrix can pick up this class and its fallback method
@Component
public class ProductServiceCall {
	@Autowired
	private RestTemplate restTemplate;
	private final static String PRODUCT_SERVICE_URL="http://product-service/api/v1/products/";
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceCall.class);
	
	@HystrixCommand(fallbackMethod = "getFallbackProductStockItem"
			//placed props in properties
//			,
//			commandProperties = {
//					@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000"),
//					@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "6"),
//					@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
//					@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000")
//					
//			}
	//,
//			//Creating Bulkhead config properties
//			threadPoolKey = "movieInfoPool",
//			threadPoolProperties = {
//					@HystrixProperty(name = "coreSize",value="20"), //size of the thread pool
//					@HystrixProperty(name = "maxQueueSize",value="10") //max size of threads in  queue
//			}
	)
	public ProductStockItem getProductStockItem(Stock stock) {
		LOGGER.info("GETTING PRODUCT USING STOCK[PRODUCT ID="+stock.getProductId()+"] WITH RESTTEMPLATE CALL TO PRODUCT-SERVICE");
		//make Rest call to product-service url with stock's productId as a param and marshall output to Product.class variables
		Product product = restTemplate
				.getForObject(PRODUCT_SERVICE_URL+stock.getProductId(), Product.class);
		LOGGER.info("CREATING PRODUCT-STOCK-ITEM USING PRODUCT ["+product+"] AND STOCK ["+stock+"] FROM STOCK-SERVICE");
		return new ProductStockItem(product.getName(),product.getPrice()
				,product.getDescription(), product.getId(),
				stock.getId(),
				stock.getNumberInStock(),
				stock.getUpdatedOn()); //making ProductStockItem with calls from product-service and stock-service and returning it	
	}
	

					/* FALLBACK METHOD*/

	public ProductStockItem getFallbackProductStockItem(Stock stock) {
		LOGGER.warn("FALLBACK FOR PRODUCT-STOCK-ITEM EXECUTED");
			return new ProductStockItem("Fallback product name", BigDecimal.ZERO, "Fallback product description",
					stock.getProductId(), stock.getId(), stock.getNumberInStock(), stock.getUpdatedOn());
		
	}
		

}
