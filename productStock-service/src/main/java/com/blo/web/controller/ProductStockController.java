package com.blo.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.blo.web.controller.hytsrix.ProductServiceCall;
import com.blo.web.controller.hytsrix.SingleStockServiceCall;
import com.blo.web.controller.hytsrix.StockServiceCall;
import com.blo.web.exception.ProductOrStockCreationError;
import com.blo.web.exception.ProductStockNotFound;
import com.blo.web.model.Product;
import com.blo.web.model.ProductStockItem;
import com.blo.web.model.Stock;
import com.blo.web.model.StockList;



@RestController
@RequestMapping(value ="api/v1")
public class ProductStockController {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	StockServiceCall stockServiceCall;
	@Autowired
	ProductServiceCall productServiceCall;
	@Autowired
	SingleStockServiceCall singleStockServiceCall;
	
	
//	@Autowired
//	private ProductStockItem productStockItem;
//	@Autowired
//	private Stock stock;
//	@Autowired
//	private Product product;
//	 
//	private HttpEntity<Object> requestBody;
//	private final static String PRODUCT_SERVICE_URL="http://product-service/api/v1/products/";
//	private static final String STOCK_SERVICE_URL="http://stock-service/api/v1/stocks/";
//	
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductStockController.class);
    
    @Value("${my.name: Config Server is not working. Please check...}")
    private String msg;
	
    
    //Endpoint to test config server
    @GetMapping(value = "/blo")
	public String getTrail(){
		return msg;
	}
	
	
	/*
	 * NB: I'm mapping stock to product because the stock class depends on product
	 * this can be seen in the class structure of stock, it has a product ID but product class doesnt have stock ID
	 */
	
	@GetMapping(value = "/productstocks")
	public List<ProductStockItem> getAllProductStockItems(){
		LOGGER.info("INITIATING GET ALL PRODUCT-STOCK ITEMS");
		//get StockList, which is a List Object, from calling the stock-service
		StockList stockList= stockServiceCall.getStockList();
		
		/*Break down stockList object to get list of stock.
		 * for each stock, get the Product from product-service using stock's productId */
		return stockList.getStockList().stream().map(
				stock->
		//make Rest call to product service url with stock's productId as a param and create new ProductStockItem
				productServiceCall.getProductStockItem(stock)		
				).collect(Collectors.toList()); //making objects a list and return this new list
	}
	

	@GetMapping(value = "/productstocks/{stockId}")
	public ProductStockItem getProductStockItem (@PathVariable(value = "stockId") int stockId){ 
		LOGGER.info("INITIATING GET A PRODUCT-STOCK ITEM [STOCK ID= "+stockId+"]");
		
		//get the Stock from calling the stock-service using the entered stockId
		Stock stock= singleStockServiceCall.getSingleStock(stockId);
		//make Rest call to product service url with stock's productId as a param and create new ProductStockItem
		return productServiceCall.getProductStockItem(stock);		
	}
	
	
	
	
	//-------- end point without Hystrix Configs
//	@GetMapping(value = "/productstocks")
//	public List<ProductStockItem> getAllProductStockItems(){
//
//		//get StockList, which is a List Object, from calling the stock-service
//		StockList stockList= restTemplate
//				.getForObject(STOCK_SERVICE_URL, StockList.class);
//		/*Break down stockList object to get list of stock.
//		 * for each stock, get the Product from product-service using stock's productId
//		 */
//		return stockList.getStockList().stream().map(
//				stock->{
//		//make Rest call to product-service url with stock's productId as a param and marshall output to Product.class variables
//			Product product = restTemplate
//					.getForObject(PRODUCT_SERVICE_URL+stock.getProductId(), Product.class);
//				return new ProductStockItem(product.getName(),product.getPrice()
//						,product.getDescription(), product.getId(),
//						stock.getId(),
//						stock.getNumberInStock(),
//						stock.getUpdatedOn()); //making ProductStockItem with calls from product-service and stock-service and returning it	
//				}
//				).collect(Collectors.toList()); //making objects a list
//												//and return this new list
//	}
	
	//-------- end point without Hystrix Configs
//	@GetMapping(value = "/productstocks/{stockId}")
//	public ProductStockItem getProductStockItem (@PathVariable(value = "stockId") int stockId){ 
//
//		//get the Stock from calling the stock-service using the entered stockId
//		Stock stock= restTemplate
//				.getForObject(STOCK_SERVICE_URL+stockId, Stock.class);
//		//get the Product from product-service using stock's productId
//		Product product = restTemplate
//				.getForObject(PRODUCT_SERVICE_URL+stock.getProductId(), Product.class);
//		
//		// map the Stock with Product object to get ProductStockItem
//		productStockItem.setProductQuantityInstock(stock.getNumberInStock());
//		productStockItem.setStockId(stock.getId());
//		productStockItem.setUpdatedDate(stock.getUpdatedOn());
//		productStockItem.setProductId(product.getId());
//		productStockItem.setProductName(product.getName());
//		productStockItem.setProductPrice(product.getPrice());
//		productStockItem.setProductDescription(product.getDescription());
//
//		return productStockItem;
//	}
	
	
	
	
	
//------------POST
	
//	@PostMapping(value="/productstocks" , consumes =  MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Object> post(@Valid @RequestBody ProductStockItem productStockItem) {
//		//disintegrate productStockItem and place in stock and product services
//			stock.setNumberInStock(productStockItem.getProductQuantityInstock());
//			product.setName(productStockItem.getProductName());
//			product.setPrice(productStockItem.getProductPrice());
//			product.setDescription(productStockItem.getProductDescription());
//			
//			// Make object an entity.
//		     requestBody = new HttpEntity<>(product);
//		     // postForObject method is used to send a request to the  Restful Service to create a data resource and return the data resource which has just been created.
//		Product derivedProduct=restTemplate.postForObject(PRODUCT_SERVICE_URL, requestBody, Product.class);
//		//null derivedProduct thrown as exception
//		if(derivedProduct==null) throw new ProductOrStockCreationError();
//		//////check for derived Product 
//		LOGGER.error(derivedProduct.toString());
//		
//		//get productId from derivedProduct and assign to stock's productID
//		stock.setProductId(derivedProduct.getId());
//				requestBody = new HttpEntity<>(stock);
//				Stock derivedStock=restTemplate.postForObject(STOCK_SERVICE_URL, requestBody, Stock.class);
//		if(derivedStock==null) { 
//			//delete derivedProduct if stock creation failed
//		//	restTemplate.delete(PRODUCT_SERVICE_URL+derivedProduct.getId());
//			throw new ProductOrStockCreationError();}
//				LOGGER.debug(derivedProduct.toString()+"\n"+derivedStock);
//
//		return new ResponseEntity<>(HttpStatus.CREATED);	
//	}
	 
	 
	
	

}
