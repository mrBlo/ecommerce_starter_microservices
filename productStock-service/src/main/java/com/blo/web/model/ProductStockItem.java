package com.blo.web.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProductStockItem {
	@NotBlank(message="Please provide a product name")
  private String productName;
	 @DecimalMin(value = "0.0", inclusive = false)
	 @Digits(integer=6, fraction=2) // 999999, 99.99
  private BigDecimal productPrice;
  private String productDescription;
	@NotNull(message = "Please provide the number of product items in stock")
  private int productQuantityInstock;
  private int productId;
  private int stockId;
  private Date updatedDate;
  
//public ProductStockItem(String productName, BigDecimal productPrice, String productDescription,
//		int productQuantityInstock) {
//	super();
//	this.productName = productName;
//	this.productPrice = productPrice;
//	this.productDescription = productDescription;
//	this.productQuantityInstock = productQuantityInstock;
//}

//need default constructor for the restTemplate.getForObject call
public ProductStockItem() {
	super();
	// TODO Auto-generated constructor stub
}

//added extra variables for post of productStockItem
public ProductStockItem(String productName,BigDecimal productPrice,
		String productDescription,int productId, int stockId, int productQuantityInstock,
		 Date updatedDate) {
	super();
	this.productName = productName;
	this.productPrice = productPrice;
	this.productDescription = productDescription;
	this.productId= productId;
	this.stockId= stockId;
	this.productQuantityInstock = productQuantityInstock;
	this.updatedDate = updatedDate;
}


	

	
	
}
