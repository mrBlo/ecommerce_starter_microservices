package com.blo.web.model;


import java.util.Date;

import lombok.Data;

//this class acts like a Feign class so it should mimick the Product class from the  product-service
@Data
public class Stock {
	
	private int id;
	private int productId;
	private int numberInStock;
	private Date updatedOn;

	//need default constructor for the restTemplate.getForObject call
	public Stock() {
		super();
	}
	
	public Stock(int id,  int productId,
			 int numberInStock,
			Date updatedOn) {
		super();
		this.id = id;
		this.productId = productId;
		this.numberInStock = numberInStock;
		this.updatedOn = updatedOn;
	}

}
