package com.blo.web.model;

import java.util.List;


import lombok.Data;

@Data
public class StockList {

	List<Stock> stockList;
	
	
}
