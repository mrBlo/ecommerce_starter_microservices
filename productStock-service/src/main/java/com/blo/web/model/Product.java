package com.blo.web.model;

import java.math.BigDecimal;

import lombok.Data;

//this class acts like a Feign class so it should mimick the Product class from the  product-service
@Data
public class Product {

	private int id;
	private String name;
	private BigDecimal price;
	private String description;
	
	//need default constructor for the restTemplate.getForObject call
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(int id, String name, BigDecimal price, String description) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
	}
	
	
	
}
