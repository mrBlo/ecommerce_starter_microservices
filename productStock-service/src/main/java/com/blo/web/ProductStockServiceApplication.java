package com.blo.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
@EnableHystrixDashboard //Hystrix dashboard can also be used in an external application that monitors circuit breakers from other apps
public class ProductStockServiceApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(ProductStockServiceApplication.class, args);
	}

}
