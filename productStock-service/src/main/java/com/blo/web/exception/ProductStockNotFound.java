package com.blo.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ProductStockNotFound extends RuntimeException{

	/** Default Serial Version UID*/
	private static final long serialVersionUID = 1L;

	//constructor
	public ProductStockNotFound() {
	    super("Stock not found");
	  }
}
