package com.blo.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductOrStockCreationError extends RuntimeException{

	/** Default Serial Version UID*/
	private static final long serialVersionUID = 1L;

	//constructor
	public ProductOrStockCreationError() {
	    super("Product Stock Operation Failed");
	  }
}
