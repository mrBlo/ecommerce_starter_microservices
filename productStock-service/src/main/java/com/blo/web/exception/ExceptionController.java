package com.blo.web.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.blo.web.model.APIError;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

	
	@ExceptionHandler(ProductStockNotFound.class)
	public ResponseEntity<APIError> handleUserNotFoundException(ProductStockNotFound ex, WebRequest request) {
		APIError errorDetails = new APIError(HttpStatus.NOT_FOUND.value(), ex.getMessage(), ex.getLocalizedMessage(),
				request.getDescription(false) + "", new Date());
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	
	//Product or Stock Creation Error in POST request
	@ExceptionHandler(ProductOrStockCreationError.class)
	public ResponseEntity<APIError> handleProductCreationException(ProductOrStockCreationError ex, WebRequest request) {
		APIError errorDetails = new APIError(HttpStatus.NOT_FOUND.value(), ex.getMessage(), ex.getLocalizedMessage(),
				request.getDescription(false) + "", new Date());
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	 /*
	  fall-back handler – catch all type of logic that deals with all other exceptions that don't have specific handlers
	  	 */
	  	@ExceptionHandler({ Exception.class })
	  	public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
	  	    APIError apiError = new APIError(
	  	      HttpStatus.INTERNAL_SERVER_ERROR.value(),  ex.getClass().getSimpleName(),
	  	      "Internal Server Error Occurred",
	  	    request.getDescription(false) + "", new Date());
	  	    return new ResponseEntity<Object>(
	  	    		apiError,HttpStatus.INTERNAL_SERVER_ERROR);
	  	}
	  	
	  	/*
		 * BindException: is thrown when fatal binding errors occur.
		 * MethodArgumentNotValidException: is thrown when argument annotated
		 * with @Valid failed validation returns HttpStatus.BAD_REQUEST as status
		 */
		@Override
		protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {

			List<String> errors = new ArrayList<String>();
			for (FieldError error : ex.getBindingResult().getFieldErrors()) {
				errors.add(error.getField() + ": " + error.getDefaultMessage());
			}
//			    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
//			        errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
//			    }

			APIError apiError = new APIError(status.value(), "Method Arguments Are Invalid", errors,
					request.getDescription(false) + "", new Date());
			return handleExceptionInternal(ex, apiError, headers, status, request);
		}

		/*
		 * MissingServletRequestPartException: is thrown when the part of a multipart
		 * request not found MissingServletRequestParameterException: is triggered when
		 * a 'required' request parameter is missing. returns HttpStatus.BAD_REQUEST as
		 * status
		 */
		@Override
		protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
				HttpHeaders headers, HttpStatus status, WebRequest request) {
			String error = ex.getParameterName() + " parameter is missing";

			APIError apiError = new APIError(status.value(), ex.getLocalizedMessage(), error,
					request.getDescription(false) + "", new Date());
			return new ResponseEntity<Object>(apiError, new HttpHeaders(), status);
		}

		/*
		 * ConstrainViolationException: Thrown when @Validated fails.
		 */
		@ExceptionHandler({ ConstraintViolationException.class })
		public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
			List<String> errors = new ArrayList<String>();
			for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
				errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": "
						+ violation.getMessage());
			}

			APIError apiError = new APIError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), errors,
					request.getDescription(false) + "", new Date());
			return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}

		/*
		 * TypeMismatchException: is thrown when you try to set bean property with wrong
		 * type. MethodArgumentTypeMismatchException: is thrown when method argument is
		 * not the expected type:
		 */
		@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
		public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
				WebRequest request) {
			String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();

			APIError apiError = new APIError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), error,
					request.getDescription(false) + "", new Date());
			return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}
		
		
		/*
	 HttpRequestMethodNotSupportedException – occurs when you send a requested with an unsupported HTTP method
		 */
		@Override
		protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
		  HttpRequestMethodNotSupportedException ex, 
		  HttpHeaders headers, 
		  HttpStatus status, 
		  WebRequest request) {
//		    StringBuilder builder = new StringBuilder();
//		    builder.append(ex.getMethod());
//		    builder.append(
//		      " method is not supported for this request. Supported methods are ");
//		    ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));
		 String supportedMethods= "Re-evaluate Request Method. Supported methods are GET, POST, PUT, DELETE, HEAD ,OPTIONS";
			
		    APIError apiError = new APIError(HttpStatus.METHOD_NOT_ALLOWED.value(), 
		     "This "+ ex.getLocalizedMessage(), //builder.toString()
		     supportedMethods,
		      request.getDescription(false) + "", new Date());
		    return new ResponseEntity<Object>(
		      apiError, HttpStatus.METHOD_NOT_ALLOWED);
		}
		
		/*
	HttpMediaTypeNotSupportedException –  occurs when the client send a request with unsupported media type
		 */
		@Override
		protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
		  HttpMediaTypeNotSupportedException ex, 
		  HttpHeaders headers, 
		  HttpStatus status, 
		  WebRequest request) {
		    StringBuilder builder = new StringBuilder();
		    builder.append(ex.getContentType());
		    builder.append(" media type is not supported. Supported media types are ");
		    ex.getSupportedMediaTypes().forEach(t -> builder.append(t + ", "));
		 
		    APIError apiError = new APIError(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), 
		      ex.getLocalizedMessage(), builder.substring(0, builder.length() - 2),
		      request.getDescription(false) + "", new Date());
		    return new ResponseEntity<Object>(
		      apiError, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		}
		
		/*
		 * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
		 */
		 @Override
		    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
		    		HttpHeaders headers, HttpStatus status, WebRequest request) {
		       // ServletWebRequest servletWebRequest = (ServletWebRequest) request;
		       // log.info("{} to {}", servletWebRequest.getHttpMethod(), servletWebRequest.getRequest().getServletPath());
		        String error = "Malformed JSON request";
		        APIError apiError = new APIError(HttpStatus.BAD_REQUEST.value(), 
		      	      ex.getLocalizedMessage(), error,
		      	      request.getDescription(false) + "", new Date());
		        return new ResponseEntity<Object>(apiError,HttpStatus.BAD_REQUEST);
		    }
		 
		 
		 /*
		  * Handle HttpMessageNotWritableException.
		  */
		  @Override
		    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		        String error = "Error writing JSON output";
		        APIError apiError = new APIError(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
			      	      ex.getLocalizedMessage(), error,
			      	      request.getDescription(false) + "", new Date());
		        return new ResponseEntity<Object>(apiError,HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  

	
}
