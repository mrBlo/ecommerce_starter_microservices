package com.blo.web;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.blo.web.model.Product;
import com.blo.web.model.ProductStockItem;
import com.blo.web.model.Stock;

import brave.sampler.Sampler;

/*
 * A class customised to contain beans to be used in application
 */
@Component
public class MyBeans {

	@Bean
	@LoadBalanced // this can also make RestTemplate provide where the service is located w/o
					// hardcoding the service url
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Bean
	public ProductStockItem getProductStockItem() {
		return new ProductStockItem();
	}

	@Bean
	public Product getProduct() {
		return new Product();
	}

	@Bean
	public Stock getStock() {
		return new Stock();
	}

	// Sleuth Sampler
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}

}
