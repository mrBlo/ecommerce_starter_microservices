# ECommerce Starter Microservices Project #

A basic collection of microservices for an eCommerce application

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Features](#features)

### General Info ###

* This project serves as a starter pack for eCommerce applications using basic microservices such as product, customer, orders etc.
* This project is strictly for *demo* purposes
* Version : v1

### Technologies ###
Project is created with:

* Spring boot 
* Maven
* Netflix Eureka
* Hystix
* Spring Cloud - Config Server & Sleuth
* Zipkin

### Features ###
 The project for now entails these microservices: 
 
* customer-service
* product-service
* order-service
* stock-service
* product-stock-service

### Future Additions
* shipping-service
* cart-service
* ratings-service


### Credits ###
* [https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/)


### Who do I talk to? ###
* Repo owner : `isaac.afrifa3@yahoo.com`
